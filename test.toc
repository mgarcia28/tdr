\babel@toc {catalan}{}
\contentsline {section}{\numberline {1}\textsc {Raó personal}}{2}{}%
\contentsline {section}{\numberline {2}\textsc {Explicació no detallada de la vàlvula}}{3}{}%
\contentsline {section}{\numberline {3}\textsc {Biografia de Nikola Tesla}}{4}{}%
\contentsline {section}{\numberline {4}\textsc {El cohet V-1}}{6}{}%
\contentsline {section}{\numberline {5}\textsc {La valvula de Tesla}}{7}{}%
\contentsline {subsection}{\numberline {5.1}\textsc {Descripcio}}{7}{}%
\contentsline {section}{\numberline {6}\textsc {Desenvolupament de la valvula}}{9}{}%
\contentsline {subsection}{\numberline {6.1}\textsc {Optimitzacio del disseny}}{12}{}%
\contentsline {subsection}{\numberline {6.2}\textsc {Demostracio amb simulacions}}{13}{}%
\contentsline {section}{\numberline {7}\textsc {Conclussio}}{14}{}%
\contentsline {section}{\numberline {8}\textsc {Annexos}}{14}{}%
\contentsline {section}{\numberline {9}\textsc {Bibliografia}}{14}{}%
